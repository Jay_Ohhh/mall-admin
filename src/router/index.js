import Vue from 'vue'
import VueRouter from 'vue-router'
import { Message } from 'element-ui'

Vue.use(VueRouter)
Vue.component(Message.name, Message)

// 将login、home、welcome组合到相同的异步块中，以下同理
const login = () =>
  import(
    /* webpackChunkName: "login_home_welcome" */ '../components/common/Login.vue'
  )
const home = () =>
  import(/* webpackChunkName: "login_home_welcome" */ '../views/Home.vue')
const welcome = () =>
  import(
    /* webpackChunkName: "login_home_welcome" */ '../components/Welcome.vue'
  )

const users = () =>
  import(
    /* webpackChunkName: "uers_rights_roles" */ '../components/users/Users.vue'
  )
const rights = () =>
  import(
    /* webpackChunkName: "uers_rights_roles" */ '../components/power/Rights.vue'
  )
const roles = () =>
  import(
    /* webpackChunkName: "uers_rights_roles" */ '../components/power/Roles.vue'
  )

const category = () =>
  import(
    /* webpackChunkName: "categoty_params" */ '../components/goods/Category.vue'
  )
const params = () =>
  import(
    /* webpackChunkName: "categoty_params" */ '../components/goods/Params.vue'
  )

const list = () =>
  import(/* webpackChunkName: "list_add" */ '../components/goods/List.vue')
const add = () =>
  import(/* webpackChunkName: "list_add" */ '../components/goods/Add.vue')

const order = () =>
  import(/* webpackChunkName: "order_report" */ '../components/order/order.vue')

const report = () =>
  import(
    /* webpackChunkName: "order_report" */ '../components/report/Report.vue'
  )

// 防止同一路由多次点击报错
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

// 后台管理系统，除了登录页面不需要token，其他页面都需要token
const routes = [
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    component: login,
  },
  {
    path: '/home',
    component: home,
    redirect: '/welcome',
    children: [
      {
        path: '/welcome',
        component: welcome,
      },
      {
        path: '/users',
        component: users,
      },
      {
        path: '/rights',
        component: rights,
      },
      {
        path: '/roles',
        component: roles,
      },
      {
        path: '/categories',
        component: category,
      },
      {
        path: '/params',
        component: params,
      },
      {
        path: '/goods',
        component: list,
      },
      {
        path: '/goods/add',
        component: add,
      },
      {
        path: '/orders',
        component: order,
      },
      {
        path: '/reports',
        component: report,
      },
    ],
  },
]

const router = new VueRouter({
  routes,
})

export default router

// 后台管理系统，除了登录页面不需要token，其他页面都需要token
/**
 * 1 如果要去的是登录页面 直接跳转
 * 2 如果不是，判断缓存有没有token
 *  1 有 跳转
 *  2 没有 重新登录获取
 */
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  if (sessionStorage.getItem('token')) {
    next()
  } else {
    Message.info('请登录')
    setTimeout(() => {
      next('/login')
    }, 1000)
  }
})
