import axios from 'axios'
// 页面顶部进度条
import NProgress from 'nprogress'

export const axios1 = axios.create({
  baseURL: 'http://127.0.0.1:8888/api/private/v1/',
  timeout: 5000,
})
// 发送请求前添加请求头Authorization字段
axios1.interceptors.request.use(config => {
  config.headers.Authorization = sessionStorage.getItem('token')
  // 展示进度条
  NProgress.start()
  return config // 必须return config
})

axios1.interceptors.response.use(config => {
  // 隐藏进度条
  NProgress.done()
  return config // 必须return config
})
